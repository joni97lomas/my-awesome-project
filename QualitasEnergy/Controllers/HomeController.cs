﻿using QualitasEnergy.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace QualitasEnergy.Controllers
{
    public class HomeController : Controller
    {
        [HttpGet]
        public ActionResult Index()
        {
            ViewBag.Title = "Login";

            return View();
        }
        [HttpPost]
        public ActionResult Index(LoginUser logged)
        {
            if (Request.HttpMethod == "POST")
            {
                LoginUser lu = new LoginUser();
                using (SqlConnection con = new SqlConnection("server=ITEM-S131306;database=QualitasEnergy;integrated security=true"))
                {
                    using (SqlCommand cmd = new SqlCommand("SP_LoginDetail", con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@FirstName", logged.FirstName);
                        cmd.Parameters.AddWithValue("@LastName", logged.LastName);
                        cmd.Parameters.AddWithValue("@Password", logged.Password);
                        cmd.Parameters.AddWithValue("@Gender", logged.Gender);
                        cmd.Parameters.AddWithValue("@Email", logged.Email);
                        cmd.Parameters.AddWithValue("@Phone", logged.Phone);
                        cmd.Parameters.AddWithValue("@SecurityAnswer", logged.SecurityAnswer);
                        cmd.Parameters.AddWithValue("@status", "INSERT");
                        con.Open();
                        ViewData["result"] = cmd.ExecuteNonQuery();
                        con.Close();
                    }
                }
            }
            return View();
        }

    }
}