﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using QualitasEnergy.Models;

namespace QualitasEnergy.Controllers
{
    public class UserLoginController : Controller
    {
        // GET: /UserLogin/  
        public string status;

        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Index(LoginViewModel loginUser)
        {
            String SqlCon = ConfigurationManager.ConnectionStrings["ConnDB"].ConnectionString;
            SqlConnection con = new SqlConnection(SqlCon);
            string SqlQuery = "select Email,Password from LoginUser where Email=@Email and Password=@Password";
            con.Open();
            SqlCommand cmd = new SqlCommand(SqlQuery, con); ;
            cmd.Parameters.AddWithValue("@Email", loginUser.Email);
            cmd.Parameters.AddWithValue("@Password", loginUser.Password);
            SqlDataReader sdr = cmd.ExecuteReader();
            if (sdr.Read())
            {
                Session["Email"] = loginUser.Email.ToString();
                return RedirectToAction("Projects");
            }
            else
            {
                ViewData["Message"] = "User Login Details Failed!!";
            }
            if (loginUser.Email.ToString() != null)
            {
                Session["Email"] = loginUser.Email.ToString();
                status = "1";
            }
            else
            {
                status = "3";
            }
            con.Close();
            return View();
        }

        [HttpGet]
        public ActionResult Projects(QualitasModel dataInfo)
        {
            QualitasModel data = new QualitasModel();
            using (QualitasEnergyEntities context = new QualitasEnergyEntities())
            {
            
                IQueryable<QualitasData> qualitasQuery = from qualit in context.QualitasData
                                                     select qualit;
                var qualitasAll = qualitasQuery.ToList();
                List<QualitasData> qualitasList = new List<QualitasData>();
                for (int i = 0; i < qualitasAll.Count; i++)
                {
                    QualitasModel uobj = new QualitasModel();
                    uobj.id = qualitasAll[i].id;
                    uobj.project_name = qualitasAll[i].project_name;
                    uobj.project_group = qualitasAll[i].project_group;
                    uobj.acquisition_date = qualitasAll[i].acquisition_date;
                    uobj.number_3l_code = qualitasAll[i].number_3l_code;
                    uobj.project_deal_type = qualitasAll[i].project_deal_type;
                    uobj.project_group = qualitasAll[i].project_group;
                    uobj.project_status = qualitasAll[i].project_status;
                    uobj.company_id = qualitasAll[i].company_id;
                    uobj.WTG_numbers = qualitasAll[i].WTG_numbers;
                    uobj.kW = qualitasAll[i].kW;
                    uobj.months_acquired = qualitasAll[i].months_acquired;

                    qualitasList.Add(uobj);

                }
                data.qualitasUserList = qualitasList;
                return View(data);
            }
        }

        [HttpGet]
        public ActionResult Users(LoginUserModel dataInfo)
        {
            LoginUserModel data = new LoginUserModel();
            using (QualitasEnergyEntities context = new QualitasEnergyEntities())
            {

                IQueryable<LoginUser> qualitasQuery = from qualit in context.LoginUser
                                                         select qualit;
                var qualitasAll = qualitasQuery.ToList();
                List<LoginUser> qualitasList = new List<LoginUser>();
                for (int i = 0; i < qualitasAll.Count; i++)
                {
                    LoginUser uobj = new LoginUser();

                    uobj.FirstName = qualitasAll[i].FirstName;
                    uobj.LastName = qualitasAll[i].LastName;
                    uobj.Email = qualitasAll[i].Email;
                    uobj.Phone = qualitasAll[i].Phone;
                    uobj.Gender = qualitasAll[i].Gender;

                    qualitasList.Add(uobj);

                }
                data.loginUserList = qualitasList;
                return View(data);
            }
        }
        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            Session.Abandon();
            return RedirectToAction("Index", "UserLogin");
        }

    }
}