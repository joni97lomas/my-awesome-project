﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Microsoft.AspNetCore.Razor.Language;
using System.Configuration;
using System.Web.Mvc;
using System.Net.Mime;

namespace QualitasEnergy.Controllers
{
    public class ValuesController : ApiController
    {

        // GET api/values
        [System.Web.Http.HttpGet]
        public ActionResult<IEnumerable<QualitasData>> GetQualitas()
        {
            using (QualitasEnergyEntities context = new QualitasEnergyEntities())
            {
                IQueryable<QualitasData> qualitasQuery = from qualit in context.QualitasData
                                                         orderby qualit.id ascending
                                                         select qualit;
                return qualitasQuery.ToList();
            }
        }

        // GET api/values/5
        [System.Web.Http.HttpGet]
        public ActionResult<QualitasData> GetQualitasById(int id)
        {
            using (QualitasEnergyEntities context = new QualitasEnergyEntities())
            {
                var qualitasById = context.QualitasData.FirstOrDefault(i => i.id == id);
                if(qualitasById == null)
                {
                    throw new Exception();
                }
                
                return qualitasById;
            }
        }

        // POST api/values
        [System.Web.Http.HttpPost]
        public ActionResult<QualitasData> qualitasPost(QualitasData qualitasEnergy)
        {
            using (QualitasEnergyEntities context = new QualitasEnergyEntities())
            {
                context.QualitasData.Add(qualitasEnergy);
                context.SaveChanges();

                return qualitasEnergy;
            }
        }

        // PUT api/values/5
        [System.Web.Http.HttpPut]
        public ActionResult<QualitasData> QualitasPut(int id, QualitasData putQualitasEnergy)
        {
            //var id = 5;
            using (QualitasEnergyEntities context = new QualitasEnergyEntities())
            {
                QualitasData qualitasEnergy = context.QualitasData.FirstOrDefault(i => i.id == id);

                qualitasEnergy.project_name = putQualitasEnergy.project_name;
                qualitasEnergy.project_number = putQualitasEnergy.project_number;
                qualitasEnergy.acquisition_date = putQualitasEnergy.acquisition_date;
                qualitasEnergy.number_3l_code = putQualitasEnergy.number_3l_code;
                qualitasEnergy.project_deal_type = putQualitasEnergy.project_deal_type;
                qualitasEnergy.project_group = putQualitasEnergy.project_group;
                qualitasEnergy.project_status = putQualitasEnergy.project_status;
                qualitasEnergy.company_id = putQualitasEnergy.company_id;
                qualitasEnergy.WTG_numbers = putQualitasEnergy.WTG_numbers;
                qualitasEnergy.kW = putQualitasEnergy.kW;
                qualitasEnergy.months_acquired = putQualitasEnergy.months_acquired;

                context.SaveChanges();

                return qualitasEnergy;
            }
        }

        // DELETE api/values/5
        [System.Web.Http.HttpDelete]
        public void deleteQualitasEnergy(int id)
        {
            using (QualitasEnergyEntities context = new QualitasEnergyEntities())
            {
                QualitasData qualitasToRemove = context.QualitasData.FirstOrDefault(i => i.id == id);
                context.QualitasData.Remove(qualitasToRemove);
                context.SaveChanges();
            }
        }
    }
}
