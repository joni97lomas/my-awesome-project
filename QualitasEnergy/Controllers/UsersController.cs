﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

namespace QualitasEnergy.Controllers
{
    public class UsersController : ApiController
    {
        [System.Web.Http.HttpGet]
        public ActionResult<IEnumerable<LoginUser>> GetUsers()
        {
            using (QualitasEnergyEntities context = new QualitasEnergyEntities())
            {
                IQueryable<LoginUser> qualitasQuery = from qualit in context.LoginUser
                                                      orderby qualit.ID ascending
                                                      select qualit;
                return qualitasQuery.ToList();
            }
        }
    }
}
