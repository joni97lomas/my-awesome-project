﻿$(document).ready(function () {
    $('#layoutMain').show();

    $('#apiTable').dataTable({
        "ajax": {
            url: "../api/users",
            content: "application/json; charset=utf-8",
            type: 'GET',
            dataSrc: 'Value',
        },
        columns: [
            { data: "FirstName" },
            { data: "LastName" },
            { data: "Email" },
            { data: "Phone" },
            { data: "Gender" },
        ]
    });

})