﻿var arrayProjectGroup = [];
var arrayProjectStatus = [];
var arrayProjectDealType = [];

$(document).ready(function () {
    $('#layoutMain').show();

    $('#addButton').click(function () {
        $('#titlePopup').html('Add Qualitas');
        $('#saveDataButton').attr('onclick', 'addNewQualitas()');
    });

    $('#close').click(function () {
        $('#tableAddEdit').find('input').val("");
        $('#saveDataButton').attr('onclick', 'saveExistingQualitas()');
    });

    $('#apiTable').dataTable({
        "ajax": {
            url: "../api/values",
            content: "application/json; charset=utf-8",
            type: 'GET',
            dataSrc: 'Value',     
        }, "initComplete": function (data) {
            $.each(data.aoData, function (key, value) {
                if (!arrayProjectGroup.includes(value._aData.project_group)) {
                    arrayProjectGroup.push(value._aData.project_group);
                }
                if (!arrayProjectStatus.includes(value._aData.project_status)) {
                    arrayProjectStatus.push(value._aData.project_status);
                }
                if (!arrayProjectDealType.includes(value._aData.project_deal_type)) {
                    arrayProjectDealType.push(value._aData.project_deal_type);
                }
            });
            getQualitasInformation();
        },
        "bProcessing": true,
        "bJQueryUI": true,
        "bScrollCollapse": true,
        "scrollX": true,
        columns: [
            { data: "project_name" },
            { data: "project_number" },
            { data: "acquisition_date" },
            { data: "number_3l_code" },
            { data: "project_deal_type" },
            { data: "project_group" },
            { data: "project_status" },
            { data: "company_id" },
            { data: "WTG_numbers" },
            { data: "kW" },
            { data: "months_acquired" },
            { data: "id", render: function (data, type, row, meta) {
                    return type === 'display' ?
                        '<a onclick="editRow(this)" id=' + data + '" ><i style="cursor:pointer" class="fa fa-edit editButton"></i></a>' :
                        data;
                }
            },
            { data: "id", render: function (data, type, row, meta) {
                    return type === 'display' ?
                        '<a onclick="deleteRow(this)" id=' + data + '" ><i style="cursor:pointer" class="fa fa-trash"></i></a>' :
                        data;
                }
            },
        ]
    });

})

//Function that is called when clicked in the delete button
function deleteRow(buttonDelete) {
    var id = parseInt($(buttonDelete).attr('id'));
    //Redirects to API function
    if (confirm("Are you sure you want to delete this entry?")) {
        ajaxDelete(id);
    }
}

//Delete rows API
function ajaxDelete(id) {
    var url = "../api/values/" + id;
    $.ajax({
        url: url,
        content: "application/json; charset=utf-8",
        type: 'DELETE',
        success: function (res) {
            reloadTable();
        }
    });
}

function getDataFromTableInformation() {
    var data = {};
    $.each($('#tableAddEdit').find('input'), function (key, value) {
        var id = $(value).attr('id');
        data[id] = $(value).val();
    });
    $.each($('#tableAddEdit').find('select'), function (key, value) {
        var id = $(value).attr('id');
        data[id] = $(value).val();
    });
    return data;
}

//Function to Add New Entry to Database API
function addNewQualitas() {
    var data = getDataFromTableInformation();
    $.ajax({
        type: "POST",
        url: "../api/values",
        data: data,
        dataType: 'json',
        success: function (data) {
            location.href = "#";
            $('#tableAddEdit').find('input').val("");
            $('#saveDataButton').attr('onclick', '');
            reloadTable();
        },
    });
}
//This function is to populate THE FIRST TIME the table to edit the data or add new API
function getQualitasInformation() {
    $.ajax({
        url: "../api/values",
        content: "application/json; charset=utf-8",
        type: 'GET',
        success: function (res) {
            var list = res.Value;
            var qualitasFirst = list[0];

            //Now we populate the info for it to be always accessible
            $.each(qualitasFirst, function (key, value) {
                var required = "";
                if (key != "acquisition_date" && key != "months_acquired") {
                    required = "required";
                }
                var inputType = "";
                if (key == 'company_id' || key == 'kW' || key == 'months_acquired') {
                    inputType = 'number';
                } else if (key == 'acquisition_date') {
                    inputType = 'datetime-local';
                } else {
                    inputType = "text";
                }
                if (key != "id" && key != "project_deal_type" && key != "project_group" && key != "project_status") {
                    var row = "<tr><td style='width:250px;'>" + capitalizeWords(key.replaceAll("_", " ")) + "</td><td style='width:250px'><input id='" + key + "'style='width:200px;margin:5px' " + required + " type='" + inputType + "'/></td></tr>"
                    $('#tableAddEdit').append(row);
                } else if (key == "project_deal_type" || key == "project_group" || key == "project_status") {
                    var row = "<tr><td style='width:250px;'>" + capitalizeWords(key.replaceAll("_", " ")) + "</td><td style='width:250px'><select id='" + key + "'style='width:200px;margin:5px'>";
                    var arrayUsed;
                    switch (key) {
                        case "project_deal_type":
                            arrayUsed = arrayProjectDealType;
                            break;
                        case "project_group":
                            arrayUsed = arrayProjectGroup;
                            break;
                        case "project_status":
                            arrayUsed = arrayProjectStatus;
                            break;
                    }
                    $.each(arrayUsed, function (key, value) {
                        row += '<option value="'+value+'">' + value + '</option>';
                    });
                    row += "</select></td></tr>";
                    $('#tableAddEdit').append(row);
                }
            })
        }
    });
}

//Function called when we click in edit button for a row
function editRow(buttonEdit) {
    var id = parseInt($(buttonEdit).attr('id'));
    getQualitasById(id);
}


//Once clicked in a edit button from a row, we get the info from DB
function getQualitasById(id) {
    let url = "../api/values/" + id;
    $.ajax({
        url: url,
        content: "application/json; charset=utf-8",
        type: 'GET',
        success: function (res) {
            let list = res.Value;
            location.href = "#popup";
            $('#titlePopup').html('Edit Qualitas');
            $('#saveDataButton').attr('onclick', 'saveExistingQualitas()');
            populateTableQualitas(list);
        }
    });
}

function saveExistingQualitas() {
    var id = $('#idQualitas').val();
    var url = "../api/values/" + id;
    $('#idQualitas').remove();
    var data = getDataFromTableInformation();

    $.ajax({
        type: "PUT",
        url: url,
        data: data,
        dataType: 'json',
        success: function (data) {
            alert('Data saved successfully.');
            location.href = "#";
            $('#tableAddEdit').find('input').val("");
            $('#saveDataButton').attr('onclick', '');
            reloadTable();
        },
    });
}

//Function to populate the popup div with the information from the get by id
function populateTableQualitas(data) {
    $.each(data, function (key, value) {
        $('#' + key).val(value);
    });
    $('#tableAddEdit').append('<input type="hidden" id="idQualitas" value="' + data.id + '" />');
}

//Capitalize each word (for the Qualitas table)
function capitalizeWords(str) {
    return str
        .toLowerCase()
        .split(' ')
        .map((word) => word.charAt(0).toUpperCase() + word.slice(1))
        .join(' ');
}

//Reload Datatable
function reloadTable() {
    $('#apiTable').DataTable().ajax.reload();
}
